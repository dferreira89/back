var express = require('express');
var bodyParser = require('body-parser');
var requestJson = require('request-json');
var bcrypt = require('bcryptjs');
var dateTime = require('node-datetime');
var app = express();
var port = process.env.port || 3000;
//var usersFile = require('./users.json');

var urlBase = "/bbvaweb/v/";
var urlBaMongo = "https://api.mlab.com/api/1/databases/dferreira/collections/";
var apiKeyMLab  ="apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";


app.use(bodyParser.json());
app.use(function(req,res,next){
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type,Accept");
  next();
})
app.listen(port);
console.log("BBVA WEB Escuchando puerto :" + port +"...");
app.post(urlBase + "login",
 function (req, res) {
   console.log("Peticion Login: "+ urlBase + "login");
   var user = req.body.email;
   var pass = req.body.password;
   var httpClient = requestJson.createClient(urlBaMongo);
   var querString = "q={'email':'" + user + "'}&f={'_id':0}&";
   console.log("Peticion Enviada: "+ querString + "login");
   httpClient.get('user?'+ querString + apiKeyMLab,function(err, respuestaMLab, body){
     console.log(req.body);
        var respget = body[0];
        console.log("pass" + pass);
        var hash1 = bcrypt.compareSync(pass, respget.password);
        console.log("hash" + hash1);
        if (respget!=undefined && hash1==true){
          respget.logged="true";
          var cambio = '{"$set":{"logged":true}}';
          console.log("q" + respget.id);
          httpClient.put('user?q={"id": ' + respget.id + '}&' + apiKeyMLab, JSON.parse(cambio),
          function(err, respuestaMLab, bodypu){
          console.log("r" + bodypu.n);
            bodypu.n >0 ? res.send({"msg":"Usuario logueado exitosamente","id":respget.id}):res.send({"msg":"Error al realizar login intente de nuevo","id":respget.id});
            console.log("Aca",respget.id);
          });
        }else {
          res.status(409);
          res.send({"msg":"error datos ingresados"});
        }
      });
});
app.post(urlBase + "logout",
  function (req, res) {
  console.log("Peticion logout: "+ urlBase + "logout");
  console.log(req.body.id);
  var user = req.body.id;
  var httpClient = requestJson.createClient(urlBaMongo);
  var querString = "q={'id':" + user + "}&f={'_id':0}&";
  httpClient.get('user?'+ querString + apiKeyMLab,
     function(err, respuestaMLab, body){
       var respget = body[0];
       if (respget!=undefined){
          delete respget.logged;
         var cambio = '{"$set":' + JSON.stringify(respget) + '}'
         httpClient.put('user?q={"id": ' + respget.id + '}&' + apiKeyMLab, respget,
         function(err, respuestaMLab, bodypu){
           bodypu.n >0 ? res.send({"msg":"Usuario deslogueado exitosamente"}):res.send({"msg":"Error al cerrra la sesion"});
         });
       }else{
         res.status(409);
         res.send({"msg":"error datos ingresados"});
       }
     });

});
function writeUserDataToFile(data){
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile("./users.json", jsonUserData, "utf8",
  function(err) {
    if(err){
      console.log(err);
    }else{
      console.log("Datos escritos en 'user.json'.");
    }
  })
}
app.get(urlBase + 'users/:id',
 function (req, res) {
   var idUsu = req.params.id;
   console.log("Get Users " + urlBase + "users/id= " + idUsu);
   var httpClient = requestJson.createClient(urlBaMongo);
   var querString = "q={'id':" + idUsu + "}&f={'_id':0}&";
   httpClient.get('user?'+ querString + apiKeyMLab, function(err, respuestaMLab, body){
     var respget=body[0];
     if(respget!=undefined){
       res.send(respget);
     }else{
       res.status(409);
       res.send({"msg":"Error al recuperar users de Mlab"})
     }
      });

});
app.get(urlBase + 'users/:idUsu/accounts/:iban',
 function (req, res) {
   var idUsu = req.params.idUsu;
   var iban = req.params.iban;
   console.log("Consulta la cuenta puntual de un usuario");
   var httpClient = requestJson.createClient(urlBaMongo);
   var querString = "q={'IBAN':" + iban + "}&f={'_id':0}&";
   httpClient.get('account?'+ querString + apiKeyMLab,
    function(err, respuestaMLab, body){
      var respget=body[0];
      if(respget!=undefined){
        res.send(body);
      }else{
        res.status(409);
        res.send({"msg":"Error al recuperar users de Mlab"})
      }
      });
});

app.get(urlBase + 'users/:idUsu/accounts',
 function (peticion, res) {
   console.log(urlBase + "lista de cuenta por usuario");
   var idUsu = peticion.params.idUsu;
   var httpClient = requestJson.createClient(urlBaMongo);
   var querString = "q={'id':" +"'"+idUsu+"'"+ "}&f={'_id':0}&";
   console.log(querString);
   httpClient.get('cards?'+ querString + apiKeyMLab, function(err, respuestaMLab, body){
     var respget=body[0];
     console.log(body);
     if(respget!=undefined){
       res.send(body);
       console.log(body);
     }else{
       console.log("ACA");
       res.status(409);
       res.send({"msg":"Error al recuperar users de Mlab"})
     }
      });
});

app.get(urlBase + 'users/:id/cards/:cardnumber/movements',
 function (req, res) {
   var id = req.params.id;
   var cardnumber = req.params.cardnumber;
   console.log(id + cardnumber);
   console.log("Lista los movimientos de una tarjeta");
   var httpClient = requestJson.createClient(urlBaMongo);
   var querString = "q={'cardnumber':" + cardnumber + "}&f={'_id':0}&";
   httpClient.get('movement?'+ querString + apiKeyMLab,
    function(err, respuestaMLab, body){
      var respget=body[0];
      console.log(respuestaMLab.statusCode);
      if(respget!=undefined){
            var decimals = 2;
            var separators = separators || ['.', "'", ','];
            for(var i=0;i<body.length;i++){
              var value=body[i].value;
              var number = (parseFloat(value) || 0).toFixed(decimals);
              var parts = number.split(/[-.]/);
              value = parts[parts.length > 1 ? parts.length - 2 : 0];
              var result = value.substr(value.length - 3, 3) + (parts.length > 1 ?
                  separators[separators.length - 1] + parts[parts.length - 1] : '');
              var start = value.length - 6;
              var idx = 0;
              while (start > -3) {
                  result = (start > 0 ? value.substr(start, 3) : value.substr(0, 3 + start))
                      + separators[idx] + result;
                  idx = (++idx) % 2;
                  start -= 3;
              }
              body[i].value=result;
            }
            res.send(body);
      }else{
        res.status(409);

        res.send({"msg":"No Existen movimientos a mostrar"})
      }
      });
});

app.get(urlBase + 'users',
 function (peticion, res) {
   console.log(urlBase + "users Peticion con Query");
   var httpClient = requestJson.createClient(urlBaMongo);
   var querString = "f={'_id':0}&";
   httpClient.get('user?'+ querString + apiKeyMLab, function(err, respuestaMLab, body){
     var respget=body[0];
     if(respget!=undefined){
       res.send(respget);
     }else{
       res.status(409);
       res.send({"msg":"Error al recuperar users de Mlab"})
     }
      });

});
app.post(urlBase + "users",
 function (req, res) {
   console.log("usuarios Peticion Post: "+ urlBase + "users");
   var querString = "f={'_id':0,'id':1}&s={'id':-1}&l=1&";
   var httpClient = requestJson.createClient(urlBaMongo);
   httpClient.get('user?'+querString + apiKeyMLab, function(err, respuestaMLab, body){
        var maximo = body[0].id+1;
        req.body.id=maximo
        console.log(req.body);
        httpClient.post('user?'+apiKeyMLab,req.body,
         function(err, respuestaMLab, bodyp){
             var respget = !err? (body[0]==undefined ?204:body) :{"msg":"Error al recuperar users de Mlab"};
             res.send(bodyp);
             console.log(err);
           });
      });


});
app.put(urlBase + 'users/:id',
 function (req, res) {
   console.log("PUT /bbvaweb/v1/users");
   var idBuscar = req.params.id;
   var updateUser = req.body;
   var existe =0;
   var cambio = '{"$set":' + JSON.stringify(req.body) + '}'
   var httpClient = requestJson.createClient(urlBaMongo);
   httpClient.put('user?q={"id": ' + req.params.id + '}&' + apiKeyMLab, JSON.parse(cambio),
    function(err, respuestaMLab, bodypu){
      bodypu.n >0 ? res.send({"msg":"Usuario actualizado"}):res.send(204);
    });
});
app.delete(urlBase + 'users/:id',
 function (req, res) {
   console.log("DELETE" + urlBase +" /bbvaweb/v");
   var idEliminar = req.params.id;
   var httpClient = requestJson.createClient(urlBaMongo);
   httpClient.put('user?q={"id": ' + req.params.id + '}&' + apiKeyMLab, [],
    function(err, respuestaMLab, bodypu){
      bodypu.removed >0 ? res.send({"msg":"Usuario Eliminado"}):res.send(204);
    });

});
app.post(urlBase + "signin",
 function (req, res) {
   console.log("usuarios Peticion Post: "+ urlBase + "users");
   var querString = "f={'_id':0,'id':1}&s={'id':-1}&l=1&";
   var httpClient = requestJson.createClient(urlBaMongo);
   httpClient.get('user?'+querString + apiKeyMLab, function(err, respuestaMLab, body1){
   var maximo = body1[0].id+1;
   req.body.id=maximo
   console.log("Peticion Sigin: "+ urlBase + "signin");
   console.log("Body"+req.body);
   var id = req.body.id;
   var first_name = req.body.first_name;
   var last_name = req.body.last_name;
   var password = req.body.password;
   console.log(password);
//   var BCRYPT_SALT_ROUNDS = 12;
//   var hashedPassword = bcrypt.hash(password, BCRYPT_SALT_ROUNDS);
   var salt = bcrypt.genSaltSync(10);
   var hash = bcrypt.hashSync(password, salt);
   req.body.password=hash;

   console.log("Pass"+hash);
   var email = req.body.email;
   var httpClient = requestJson.createClient(urlBaMongo);
   var querString = "{'id':'" + id + "','first_name':'" + first_name + "','last_name':'" + last_name + "','email':'" + email + "','password':'" + password + "'}&";
   console.log("Peticion id: "+ querString + "signin");
   httpClient.post('user?'+ apiKeyMLab,req.body,function(err, respuestaMLab, body){
     console.log(req.body);
     console.log("aca");
     res.send({"msg":"Usuario Creado"});

});
       });
       });
app.post(urlBase + "crear",
  function (req, res) {
    console.log("usuarios Peticion Post: "+ urlBase + "users");
    var id = req.body.id;
    console.log("Cliente " + id);
    function makeid(length) {
    var result           = '';
    var characters       = '0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
     return result;
    }
    var cardnumber=makeid(16);
    req.body.cardnumber=cardnumber;
    var password = req.body.password;
    console.log(password);
//   var BCRYPT_SALT_ROUNDS = 12;
//   var hashedPassword = bcrypt.hash(password, BCRYPT_SALT_ROUNDS);
    var salt = bcrypt.genSaltSync(10);
    var hash = bcrypt.hashSync(password, salt);
    req.body.password=hash;
    console.log("Pass"+hash);
    var httpClient = requestJson.createClient(urlBaMongo);
    httpClient.post('cards?'+ apiKeyMLab,req.body,function(err, respuestaMLab, body){
      console.log(req.body);
      console.log("aca");
      res.send({"msg":"Tarjeta Creada"});
});
});
app.post(urlBase + "avanzar",
  function (req, res) {
    console.log("usuarios Peticion Post: "+ urlBase + "users");
    var id = req.body.id;
    var cardnumber = req.body.cardnumber;
    var pass = req.body.password;
//    var date = new Date();
//    var timestamp = date.getTime();
    var dt = dateTime.create();
    var formatted = dt.format('Y-m-d H:M:S');
    console.log(formatted);
    req.body.timestamp = formatted;
    function makeid(length) {
    var result           = '';
    var characters       = '0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
     return result;
    }
    var movnumber=makeid(4);
    req.body.movnumber=movnumber;
    console.log("Cliente " + id + cardnumber);
    var httpClient = requestJson.createClient(urlBaMongo);
    var querString = "q={'cardnumber':'" + cardnumber + "','id':'" + id + "'}&f={'_id':0}&";
    console.log("Peticion Enviada: "+ querString + "avanzar");
    httpClient.get('cards?'+ querString + apiKeyMLab,function(err, respuestaMLab, body){
      console.log(body);
         var respget = body[0];
         console.log("pass" + pass);
         var hash1 = bcrypt.compareSync(pass, respget.password);
         console.log("hash" + hash1);
         if (respget!=undefined && hash1==true){
           var httpClient = requestJson.createClient(urlBaMongo);
           httpClient.post('movement?'+ apiKeyMLab,req.body,function(err, respuestaMLab, body){
             console.log(req.body);
             console.log("aca");
             res.send({"msg":"Avance Creado"});
           });
         }else {
           res.status(409);
           res.send({"msg":"clave incorrecta"});
         }
       });
        });
